package pageElements;

import org.openqa.selenium.By;

public interface Quote {
	public static By newOppRelAcc = By.xpath("//input[@name='new_opportunity_related_accounts']");
	public static By newQuote = By.xpath("//input[@name='new00N3000000B5zfL']");
	public static By accRole = By.id("thepage:theblock:thesection:j_id32:j_id36");
	public static By NewQuotation = By.xpath("//input[@title='New Quotation']");
	public static By RepInformation = By.id("quotePage:theForm:theBlock:customerInfo:repSelected");
	public static By customerInformation = By.id("quotePage:theForm:theBlock:customerInfo:contactSelected2");
	public static By quotestatus = By.xpath("//select[@id='quotePage:theForm:theBlock:j_id75:j_id88']");
	public static By quoteType = By.xpath("//select[@id='quotePage:theForm:theBlock:j_id75:j_id92']");
	public static By sourceofEnquiry = By.xpath("//select[@id='quotePage:theForm:theBlock:j_id75:j_id94']");
	public static By region = By.xpath("//select[@id='quotePage:theForm:theBlock:j_id75:quoteRegion']");
	public static By drawingDate = By.xpath("//input[@id='quotePage:theForm:theBlock:j_id105:j_id110']");
	public static By alternate = By.xpath("//textarea[@id='quotePage:theForm:theBlock:notesSection:j_id116']");
	public static By addenda =By.xpath("//textarea[@id='quotePage:theForm:theBlock:notesSection:j_id119']");
	public static By parentLineChkBx = By.xpath("(//input[@name='_line_item_list'])[1]");
	public static By overageChkBx = By.id("viewColumns_t0");
	public static By priceDetails = By.xpath("//span[contains(text(),'Pricing Details')]");
	public static By copyLineItemsBtn = By.id("copy_line_items");
	public static By noOfCopiestxtBx = By.id("num_of_copies2");
	public static By CpyLnItempopupOKbtn = By.xpath("//img[@class='ok-button-icon']");
	
	public static By cpqlineButton = By.xpath("//input[@name='cpq_lines']");
	public static By cpqLinesSSO_Button = By.xpath("//input[@value='CPQ Lines SSO']");
	public static By cpqusername = By.name("username");
	public static By cpqpassword = By.id("psword");
	public static By loginBtn = By.id("log_in");
	
	public static By oktaUserID_TextBox = By.xpath("//input[@id='okta-signin-username']");
	public static By oktaPasswd_TextBox = By.xpath("//input[@id='okta-signin-password']");
	public static By oktaLogin_Button = By.xpath("//input[@id='okta-signin-submit']");
	public static By transactionName_TextBox = By.xpath("//input[@id='transactionName_t']");
	public static By partnerProfile = By.xpath("//div[@class='nav-links']/a[3]/img");
	
	public static By cpqaddlineitembtn = By.xpath("(//a[@id='add_line_item'])[1]");
	public static By cpqcctlink = By.xpath("//a[contains(text(),'CCT')]");
	public static By cpqcubCurtTrack = By.xpath("//a[contains(text(),'Cubicle Curtains and Track')]");
	public static By configureCubCurtTrack = By.xpath("//a[contains(text(),'Configure Cubicle Track')]");
	public static By partsSearch =By.xpath("//a[@class='parts-search-toggle']");
	public static By partSearchTxtbox = By.name("q");
	public static By searchButton = By.name("submitButton");
	public static By partsearchResult = By.xpath("//input[@name='bm_cm_part_id']");
	public static By addtoTransaction = By.id("add_to_transaction");
	public static By saveBtn = By.id("save");
	public static By submitBtn =By.id("submit");
	public static By recievePO = By.id("receive_po");
	public static By fullfillmentDrpDwn = By.xpath("//select[@name='_36244118_2_fulfillmentType_line']");
	public static By copyFulfilmentTypeBtn = By.id("copy_fulfillment_type");
	public static By transactionDetailsTab = By.xpath("//span[@class='tab-inner']/span[contains(text(),'Transaction Details')]");
	public static By taxCalculationTab = By.xpath("//span[@class='tab-inner']/span[contains(text(),'Tax Calculation')]");
	public static By OrderTab = By.xpath("//span[@class='tab-inner']/span[contains(text(),'Order')]");
	public static By CustomerDetailsTab = By.xpath("//span[@class='tab-inner']/span[contains(text(),'Customer Details')]");
	public static By CalculateCommisionBtn = By.id("calculate_commission");
	public static By CommisionsTab = By.xpath("(//span[contains(text(),'Commissions')])[1]");
	public static By EstimatedCommision = By.id("estimatedCommission_quote-display");
	public static By isCommissionable = By.name("isCommissionable_t");
	public static By paymentTerms = By.name("paymentTerms_t");
	public static By reviseBtn = By.id("revise");
	public static By requestedDate = By.xpath("//input[@id='requestedDate_quote']");
	public static By submittedMailDate = By.xpath("//input[@id='draftNeedByDate_t']");
	
	
	public static By CustomerPO = By.xpath("//input[@id='customerPONbr_quote']");
	public static By tranTabrequesttype = By.xpath("//select[@name='requestType_quote']");
	public static By requestDate = By.xpath("//input[@id='requestedDate_quote']");
	public static By calculateTaxBtn = By.id("calculate_tax");
	public static By OrdTabOrderType = By.xpath("//select[@name='orderType_quote']");
	public static By OrdTabSBU = By.xpath("//select[@name='sBU_t']");
	public static By SendSOA = By.xpath("//select[@name='sendSOA_quote']");
	public static By SOAContact = By.xpath("//input[@name='sOAContact_quote']");
	public static By SendPriceLineDetails = By.xpath("//select[@name='sendPriceLineDetails_t']");
	public static By frieghtTerms = By.xpath("//select[@name='freightTerms_t']");
	public static By createOrderBtn = By.id("create_order");
	public static By shipFrom = By.xpath("//select[@name='shipFrom']");
	public static By totalTaxValue = By.xpath("//input[@id='totalTax_t']");
	
	public static By shipToSite = By.xpath("//input[@id='_shipTo_t_company_name']");
	public static By shipToAddress = By.xpath("//input[@id='_shipTo_t_address']");
	public static By shipToAddress2 = By.xpath("//input[@id='_shipTo_t_address_2']");
	public static By shipToCity = By.xpath("//input[@id='_shipTo_t_city']");
	public static By shipToState = By.xpath("//input[@name='_shipTo_t_state_text']");
	public static By shipToCountry = By.xpath("//input[@id='_shipTo_t_country']");
	public static By shiptoZip = By.xpath("//input[@id='_shipTo_t_zip']");
	
	public static By BillToSite = By.xpath("//input[@id='_invoiceTo_t_company_name']");
	public static By BillToAddress = By.xpath("//input[@id='_invoiceTo_t_address']");
	public static By BillToAddress2 = By.xpath("//input[@id='_invoiceTo_t_address_2']");
	public static By BillToCity = By.xpath("//input[@id='_invoiceTo_t_city']");
	public static By BillToState = By.xpath("//input[@name='_invoiceTo_t_state_text']");
	public static By BillToCountry = By.xpath("//input[@id='_invoiceTo_t_country']");
	public static By BilltoZip = By.xpath("//input[@id='_invoiceTo_t_zip']");
	public static By ContactDrop = By.id("thepage:theblock:thesection:contactsection:conselect");
	public static By AccntTxtbox = By.xpath("//input[@id='thepage:theblock:thesection:accsection:accfield']");
	
	public static By shipToCustomer = By.xpath("//input[@name='shipToCustomer_t']");
	public static By FetchshiptoCustomer = By.id("fetch_ship_to_customers");
	public static By BillToCustomer = By.xpath("//input[@id='billToCustomer_t']");
	public static By FetchBillToCustomer = By.id("fetch_bill_to_customers");
	public static By shipToCustomerSearchBtn = By.xpath("//input[@id='shipToCust']");
	public static By billToCustomerSearchBtn = By.id("billToCust");
	public static By ShipToLookupButton = By.xpath("//input[@id='shiptolookupbutton']");
	public static By billToLookUpButton = By.xpath("//input[@id='billtolookupbutton']");
	public static By FetchSiteDetailsButton = By.xpath("//a[@id='fetch_site_details']");
	public static By resetShipTo = By.xpath("//a[@id='clear_ship_to_fields']");
	public static By resetBillTo = By.xpath("//a[@id='clear_bill_to_fields']");
	
	public static By repIDTxtBox = By.xpath("//input[@id='orderRepID']");
	public static By specRepID = By.xpath("//input[@id='specRepID_quote']");
	public static By Orderstatus = By.xpath("//img[@src='/bmfsweb/constructionspectest/image/images/CS_7_OC.png']");
	public static By refreshlink = By.xpath("//a[contains(text(),'Refresh')]");
	public static By homeBtn = By.xpath("//img[@title='Home']");
	public static By cubicalCurtainDrpDwn = By.id("product-nav-CCT");
	public static By tracktypeDrpDwn = By.id("trackType");
	public static By configuratorTypDrpDwn = By.id("configurationType");
	public static By trackColorDrpDwn = By.id("trackColor");
	public static By shippingMethodDrpDwn = By.id("shippingMethod");
	public static By noOfTrackersTxtBx = By.id("nbrOfTracks");
	public static By trackQtyTxtbx = By.id("trackQty-0");
	public static By trackShapeDrpDwn = By.id("trackShape-0");
	public static By L1LengthFt = By.id("l1LengthFt-0");
	public static By L1LenghtIn = By.id("l1Length-0");
	public static By CurtainFullness = By.id("curtainFullness-0");
	public static By FloorTxtBx = By.id("floor-0");
	public static By roomTxtBx = By.id("room-0");
	public static By updateBtn = By.id("update");
	public static By startOverBtn = By.id("start_over");
	public static By requestType_DropDown = By.name("requestType_quote");
	public static By requestDate_DateField = By.xpath("//input[@id='requestedDate_quote']");
	public static By draftNeedDate_DateField = By.xpath("//input[@id='draftNeedByDate_t']");
	public static By incoTermsCode_DropDown = By.name("fOBTerms_t");
}
