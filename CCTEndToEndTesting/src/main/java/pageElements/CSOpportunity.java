package pageElements;

import org.openqa.selenium.By;

public interface CSOpportunity {
	public static By opportunityNameTXT = By.xpath("//input[@id='00N3000000B5ndZ']");
	public static By csProductedAddedDrp = By.xpath("//select[@id='00N3000000B5zSF']");
	public static By projectTypeDrp_Opp = By.xpath("//select[@id='00N3000000B8IDN']");
	public static By stageDrpDwn = By.xpath("//select[@id='00N3000000B5ncm']");
	public static By otrtcurtains = By.xpath("//select[@id='00N3000000CKXwz']");
	public static By quoteIncludes = By.xpath("//select[@id='00N3000000CKXwu']");
	public static By requestType = By.xpath("//select[@id='00N3000000B9PIj']");
	public static By specificationLevel = By.xpath("//select[@id='00N3000000B5zSA']");
	public static By csOppSaveBtn = By.xpath("(//input[@title='Save'])[1]");
	public static By projectRefField = By.xpath("(//img[@class='lookupIcon'])[1]");
}
