package pageElements;

import org.openqa.selenium.By;

public interface SuperScreen {
	
	public static By username = By.id("username");
	public static By password = By.id("password");
	public static By signInBtn = By.id("Login");
	public static By CSLogo = By.xpath("//img[@alt='cs Making Buildings Better']");
	public static By hp_search_txt = By.xpath("//input[@id='phSearchInput']");
	public static By manageExternalUser_drpdwn = By.xpath("//div[@id='workWithPortalButton']");

}
