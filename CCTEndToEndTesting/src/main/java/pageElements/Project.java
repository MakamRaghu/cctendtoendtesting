package pageElements;

import org.openqa.selenium.By;

public interface Project {
	public static By new_btn = By.xpath("//input[@name='new']");
	public static By projectNameTxt = By.xpath("//input[@id='Name']");
	public static By projectStatusDrp = By.xpath("//select[@id='00N30000008bC3r']");
	public static By projectTypeDrp = By.xpath("//select[@id='00N30000008bC3w']");
	public static By marketSectorDrp = By.xpath("//select[@id='00N30000008bCI4']");
	public static By secondaryMarketType = By.xpath("//select[@id='00N30000008bCI9']");
	public static By cityTownTxt = By.xpath("//input[@id='00N30000008biDt']");
	public static By stateDrp = By.xpath("//select[@id='00N30000008biEI']");
	public static By postalcodeTxt = By.xpath("//input[@id='00N30000008biEN']");
	public static By countryDrp = By.xpath("//select[@id='00N30000008biE8']");
	public static By streetTxtarea = By.xpath("//textarea[@id='00N30000008biDe']");
	
	public static By projectName_Proj = By.xpath("//div[@id='Name_ileinner']");
	public static By recentProjectName = By.xpath("//div[@class='pbBody']/table/tbody/tr/th[1]/a");
	
	
	public static By save_btn1 = By.xpath("//td[@class='pbButton']/input[@name='save']");
	public static By cpqLines = By.xpath("//input[@name='cpq_lines']");
	public static By cpqUsername = By.xpath("//input[@id='username']");
	public static By cpqPassword = By.xpath("//input[@id='psword']");
	public static By cpqLoginBtn = By.id("log_in");
	public static By customerDetailsTab = By.xpath("//span[text()='Customer Details']");
}
