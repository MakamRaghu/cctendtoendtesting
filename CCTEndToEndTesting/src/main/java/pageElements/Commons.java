package pageElements;

import org.openqa.selenium.By;

public interface Commons {
	public static By edit_btn1 = By.xpath("//td[@class='pbButton']/input[@name='edit']");
	public static By saveNew_btn1 = By.xpath("//td[@class='pbButton']/input[@name='save_new']");
	public static By cancel_btn1 = By.xpath("//td[@class='pbButton']/input[@name='cancel']");
	public static By RecordlookUp_Cont = By.xpath("//div[@class='requiredInput']/span/a/img");
	public static By tableElements = By.xpath("//div[@class='pbBody']/table/tbody/tr/th[1]/a");
}
