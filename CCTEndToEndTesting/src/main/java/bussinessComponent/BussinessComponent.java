package bussinessComponent;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import jxl.read.biff.BiffException;
import pageElements.CSOpportunity;
import pageElements.Project;
import pageElements.Quote;

public class BussinessComponent extends ReusableFunctions implements Project,CSOpportunity,Quote{
	public String project=null;
	public String OppurtunityName = null;
	public String ConfigPrice=null;
	public void createProjects(String PrjName) throws BiffException, IOException, InterruptedException {
		System.out.println("****************************************Creating Project*************************************************\n");
		WebDriverWait wait = new WebDriverWait(driver, 180);
		wait.until(ExpectedConditions.visibilityOfElementLocated(new_btn));
		driver.findElement(new_btn).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(projectNameTxt));
		SendTextIntoTextBox(projectNameTxt, PrjName);
		selectValueFromDropDownUsingValue(projectStatusDrp, "Pending");
		selectValueFromDropDownUsingValue(projectTypeDrp, "New Construction");
		selectValueFromDropDownUsingValue(marketSectorDrp, "Retail");
		selectValueFromDropDownUsingValue(secondaryMarketType, "Food and Beverage");
		driver.findElement(cityTownTxt).sendKeys("San Fransico");
		selectValueFromDropDownUsingValue(stateDrp, "New Jersey");
		driver.findElement(postalcodeTxt).sendKeys("07302");
		driver.findElement(streetTxtarea).sendKeys("California,street 2");
		driver.findElement(save_btn1).click();
		project = driver.findElement(projectName_Proj).getText();
		if (project.equals(PrjName)) {
			System.out.println("Project created with name " + project );
			takeScreenShoot(project);
		} else {
			System.out.println("Project not created");
		}
		System.out.println("\n****************************************End created Project*************************************************");
	}
	
	public void createOpportunities(String oppname) throws BiffException, IOException, InterruptedException {
		System.out.println("****************************************Creating Oppurtunity*************************************************\n");
		waitUntill_visibilityOfElement(By.xpath("//input[@value='New CS Opportunity']"), 200);
		clickElement(By.xpath("//input[@value='New CS Opportunity']"));
		SendTextIntoTextBox(opportunityNameTXT, oppname);
		selectValueFromDropDownUsingValue(projectTypeDrp_Opp, "New Construction");
		selectValueFromDropDownUsingValue(specificationLevel,"Basis of Design");
		selectValueFromDropDownUsingValue(quoteIncludes,"Curtains Only");
		selectValueFromDropDownUsingValue(otrtcurtains,"OTRT");
		selectValueFromDropDownUsingValue(stageDrpDwn, "Bidding");
		selectValueFromDropDownUsingValue(requestType, "ITB");
		//BID date
		driver.findElement(By.xpath("//input[@id='00N3000000B7bUw']/ancestor::span/span/a")).click();
		driver.findElement(By.xpath("(//option[@value='0'])[1]")).click();
		driver.findElement(By.id("00N3000000B9Et5_right_arrow")).click();
		driver.findElement(csOppSaveBtn).click();
		
		OppurtunityName = driver.findElement(By.id("00N3000000B5ndZ_ileinner")).getText();
		if(OppurtunityName.equals(oppname)) {
			System.out.println("oppurtunity created with Name "+ oppname);
			takeScreenShoot(OppurtunityName);
		}else {
			System.out.println("oppurtunity is not created");
		}
		System.out.println("\n****************************************End created Oppurtunity*************************************************");
	}
	public void createQuot() throws InterruptedException {
		driver.findElement(newQuote).click();
		Thread.sleep(2000);
		selectValueFromDropDownUsingVisibleText(RepInformation, "New Jersey Sales / Representative - Bidding / 112");
		Thread.sleep(2000);
		selectValueFromDropDownUsingVisibleText(customerInformation, "New Jersey Sales / Architect - Consulting");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(quotestatus, "Pending");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(quoteType, "PPS");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(sourceofEnquiry, "Representative");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(region, "MD-CC");
		Thread.sleep(2000);
		driver.findElement(drawingDate).sendKeys("Drawing Date");
		Thread.sleep(2000);
		driver.findElement(alternate).sendKeys("alternate");
		Thread.sleep(2000);
		driver.findElement(addenda).sendKeys("addenda");
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@value='Save'])[1]")).click();
		Thread.sleep(2000);
	}
	public void switchingWindowAndHand(String frameIDorName, By tableelement, String record) {
		String parentWindow = driver.getWindowHandle();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!parentWindow.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
				driver.switchTo().frame(frameIDorName);
				clickrecordFromTable(tableelement, record);
				break;
			}
		}
		driver.switchTo().window(parentWindow);
	}
	@Override
	public void clickrecordFromTable(By elements, String record) {
		List<WebElement> li = driver.findElements(elements);
		for(WebElement records : li) {
			if(records.getText().contains(record)) {
				records.click();
				break;
			}
		}
	}
	public void cpqlinescreate(String projectname) throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		oktaLoginAndGettingTransactionName();
		configureTheCCTAndAddLineItems(projectname,"9");
		transactionTab();
		commisionTab(projectname);
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.switchTo().frame("itarget");
		Thread.sleep(2000);
		waitUntill_elementIsClickable(CustomerDetailsTab, 200);
		clickElementUsingJS(CustomerDetailsTab);
		Thread.sleep(4000);
		waitUntill_visibilityOfElement(resetShipTo, 200);
		clickElementUsingJS(resetShipTo);
		Thread.sleep(2000);
		clickElementUsingJS(resetBillTo);
		Thread.sleep(4000);
		gettingValuesUsingJSforshipTOCustomer();
		Thread.sleep(2000);
		gettingValuesUsingJSforBillTOCustomer();
		Thread.sleep(2000);
		waitUntill_elementIsClickable(FetchSiteDetailsButton, 200);
		waitUntill_visibilityOfElement(FetchSiteDetailsButton, 200);
		clickElementUsingJS(FetchSiteDetailsButton);
		Thread.sleep(6000);
		gettingValuesUsingJSforShipTOSite();
		Thread.sleep(2000);
		gettingValuesUsingJSforBillTOSite();
		waitUntill_visibilityOfElement(saveBtn, 200);
		clickElementUsingJS(saveBtn);
		Thread.sleep(6000);
		
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
		driver.switchTo().frame("itarget");
		waitUntill_visibilityOfElement(taxCalculationTab, 200);
		executor.executeScript("arguments[0].click();", driver.findElement(taxCalculationTab));
		waitUntill_visibilityOfElement(shipFrom, 200);
		selectValueFromDropDownUsingValue(shipFrom, "MX2");
		Thread.sleep(2000);
		if(driver.findElement(By.id("billTocustomerNumber_t")).getText().isEmpty()) {
			SendTextIntoTextBox(By.id("billTocustomerNumber_t"), "1000418");
			Thread.sleep(2000);	
		}
		waitUntill_visibilityOfElement(calculateTaxBtn, 200);
		String taxCal1 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		driver.findElement(calculateTaxBtn).click();
		waitUntill_visibilityOfElement(totalTaxValue, 200);
		String totalTax = driver.findElement(totalTaxValue).getAttribute("value");
		System.out.println(projectname+" Total Tax value after calculated ..... "+totalTax);
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.switchTo().frame("itarget");
		Thread.sleep(2000);
		WebElement ordertab = driver.findElement(OrderTab);
		executor.executeScript("arguments[0].scrollIntoView()", ordertab); 
		executor.executeScript("arguments[0].click();", ordertab);
		Thread.sleep(4000);
		waitUntill_visibilityOfElement(OrdTabOrderType, 200);
		waitUntill_elementIsClickable(CustomerPO, 200);
		driver.findElement(CustomerPO).sendKeys("PO888969");
		selectValueFromDropDownUsingValue(requestType_DropDown, "shipOn");
		SendTextIntoTextBox(requestedDate, "01/05/2020");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(incoTermsCode_DropDown, "FCA");
		selectValueFromDropDownUsingValue(OrdTabOrderType, "Standard");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(OrdTabSBU, "CCT");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(SendSOA, "Y");
		Thread.sleep(2000);
		driver.findElement(SOAContact).sendKeys("rmakam@c-sgroup.com");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(SendPriceLineDetails, "Y");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(frieghtTerms, "Add freight");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(paymentTerms, "IMMEDIATE");
		Thread.sleep(2000);
		waitUntill_visibilityOfElement(createOrderBtn, 200);
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		System.out.println(projectname+" Timestamp at clicking create Order   "+timeStamp);
		Reporter.log(projectname+" Timestamp at clicking create Order   "+timeStamp);
		log.info(projectname+" Timestamp at clicking create Order   "+timeStamp);
		driver.findElement(createOrderBtn).click();
		scrollupAndDown(refreshlink);
		Thread.sleep(3000);
		String timeStamp2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		System.out.println(projectname+" Time took to create Order . .. . ."+timeStamp2);
		Reporter.log(projectname+" Time took to create Order . .. . ."+timeStamp2);
		log.info(projectname+" Time took to create Order . .. . ."+timeStamp2);
		Reporter.log(projectname+" Time taken for creating order "+timeStampDiff(timeStamp,timeStamp2));
		System.out.println(projectname+" Time taken for creating order "+timeStampDiff(timeStamp,timeStamp2));
		Thread.sleep(2000);
		waitUntill_visibilityOfElement(OrderTab, 200);
		clickElementUsingJS(OrderTab);
		Thread.sleep(2000);
		waitUntill_visibilityOfElement(By.xpath("//span[@id='readonly_1_orderNumber_quote']"), 200);
		Thread.sleep(2000);
		String OrderNum = driver.findElement(By.xpath("//span[@id='readonly_1_orderNumber_quote']")).getText();
		System.out.println(projectname+"  Order number is   "+OrderNum);
		Reporter.log(projectname+"  Order number is   "+OrderNum);
		log.info(projectname+"  Order number is   "+OrderNum);
	}
	
	public void gettingValuesUsingJSforshipTOCustomer() {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		//String shipData = js.executeAsyncScript("document.getElementById('shipToCustomerListFromSFDC_t').value").toString();
		//System.out.println(shipData); 
		js.executeScript("document.getElementById('shipToCustomer_t').value='JCO & Associates TEST'");
		js.executeScript("document.getElementById('shipToCustomerAccId_t').value='0012F00000eDcdVQAS'");
		js.executeScript("document.getElementById('shipToPartyID_t').value='300000266727236'");
		js.executeScript("document.getElementById('shipToCustAccId_t').value='300000266697117'");
	}
	public void gettingValuesUsingJSforBillTOCustomer() {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		//String shipData = js.executeAsyncScript("document.getElementById('shipToCustomerListFromSFDC_t').value").toString();
		//System.out.println(shipData); 
		js.executeScript("document.getElementById('billToCustomer_t').value='JCO & Associates TEST'");
		js.executeScript("document.getElementById('billToCustomerAccId_t').value='0012F00000eDcdVQAS'");
		js.executeScript("document.getElementById('invoiceToPartyID_t').value='300000266727236'");
		js.executeScript("document.getElementById('billToCustAccId_t').value='300000266697117'");
		js.executeScript("document.getElementById('shipToCustomerNumber_t').value='1000429'");
	}
	public void gettingValuesUsingJSforShipTOSite() {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		//String shipData = js.executeAsyncScript("document.getElementById('shipToCustomerListFromSFDC_t').value").toString();
		//System.out.println(shipData); 
		js.executeScript("document.getElementById('_shipTo_t_company_name').value='Test 2'");
		js.executeScript("document.getElementById('_shipTo_t_company_name').value='Test 2'");
		js.executeScript("document.getElementById('_shipTo_t_address').value='AD'");
		js.executeScript("document.getElementById('_shipTo_t_address_2').value='Add1'");
		js.executeScript("document.getElementById('_shipTo_t_city').value=''");
		js.executeScript("document.getElementById('_shipTo_t_state').value='New York'");
		js.executeScript("document.getElementsByName('_shipTo_t_state_text')[0].value='New York'");
		js.executeScript("document.getElementById('_shipTo_t_country').value='United States'");
		js.executeScript("document.getElementById('_shipTo_t_zip').value='10001'");
		js.executeScript("document.getElementById('shipToPartySiteID_t').value='300000266699116'");
		js.executeScript("document.getElementById('shipToPartyID_t').value='300000266727236'");
		js.executeScript("document.getElementById('shipToSiteUseID_t').value='300000266727242'");
	}
	public void gettingValuesUsingJSforBillTOSite() {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		//String shipData = js.executeAsyncScript("document.getElementById('shipToCustomerListFromSFDC_t').value").toString();
		//System.out.println(shipData); 
		js.executeScript("document.getElementById('_invoiceTo_t_company_name').value='Test'");
		js.executeScript("document.getElementById('_invoiceTo_t_company_name').value='Test'");
		js.executeScript("document.getElementById('_invoiceTo_t_address').value=''");
		js.executeScript("document.getElementById('_invoiceTo_t_address_2').value='PO BOX 618'");
		js.executeScript("document.getElementById('_invoiceTo_t_city').value='PORTLAND'");
		js.executeScript("document.getElementById('_invoiceTo_t_state').value='Mississippi'");
		js.executeScript("document.getElementsByName('_invoiceTo_t_state_text')[0].value='Mississippi'");
		js.executeScript("document.getElementById('_invoiceTo_t_country').value='United States'");
		js.executeScript("document.getElementById('_invoiceTo_t_zip').value='04104'");
		js.executeScript("document.getElementById('invoiceToPartySiteID_t').value='300000266698125'");
		js.executeScript("document.getElementById('invoiceToPartyID_t').value='300000266727236'");
		js.executeScript("document.getElementById('billToSiteUseID_t').value='300000266723244'");
	}
	public void switchAndHande() throws InterruptedException, AWTException, IOException {
		String parentWindow = driver.getWindowHandle();
		System.out.println("parent window ..."+driver.getCurrentUrl());
		Set<String> set = driver.getWindowHandles();
		Iterator<String> i1 = set.iterator();
		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!parentWindow.equalsIgnoreCase(ChildWindow)) {
				System.out.println("Switching to child window");
				driver.switchTo().window(ChildWindow);
				System.out.println("Switched to child window");
				Thread.sleep(2000);
				//System.out.println("child window ..."+driver.getCurrentUrl());
				//driver.findElement(By.xpath("//td[text()=1000418]/ancestor::tr/td[1]/a")).sendKeys(Keys.ENTER);
				clickElementUsingJS(By.xpath("//td[text()=1000418]/ancestor::tr/td[1]/a"));
				//clickElement(By.xpath("//a[starts-with(text(),'JCO') and substring(text(),7)='Associates']"));
		        Thread.sleep(3000);
		        break;	
			}
		}
		driver.switchTo().window(parentWindow);
	}
	
	public void robotClass() throws InterruptedException, AWTException {
		Thread.sleep(3000);
		String parentWindow = driver.getWindowHandle();
		Set<String> set = driver.getWindowHandles();
		Iterator<String> i1 = set.iterator();
		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!parentWindow.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
				Thread.sleep(2000);
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_TAB);	
		        Thread.sleep(2000);
		        robot.keyPress(KeyEvent.VK_ENTER);
		        Thread.sleep(2000);
		        robot.keyRelease(KeyEvent.VK_TAB);
		        Thread.sleep(1000);
		        robot.keyRelease(KeyEvent.VK_ENTER);
		        break;
			}
		}
		driver.switchTo().window(parentWindow);
	}
	public void winhandlewithIndex(int index) {
		String mainwin=null;
		Set<String> windSet = driver.getWindowHandles();
		Iterator<String> intera = windSet.iterator();
		for(int i=1;i<index;i++) {
			mainwin = intera.next();
			System.out.println(mainwin);
		}
		driver.switchTo().window(mainwin);
		List<WebElement> elem = driver.findElements(By.xpath("//tr/td/a"));
		for(WebElement oreo:elem) {
			System.out.println("get text from new tab   "+oreo.getText());
		}
	}
	public void switchingWindowAndHand(String text) throws InterruptedException {
		String parentWindow = driver.getWindowHandle();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!parentWindow.equalsIgnoreCase(ChildWindow)) {
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				driver.switchTo().window(ChildWindow);
				Thread.sleep(3000);
				driver.findElement(By.xpath("//td/a[contains(text(),'"+text+"')]")).click();
				Thread.sleep(2000);
				break;
			}
		}
		driver.switchTo().window(parentWindow);
	}
	public void transactionTab() throws InterruptedException, IOException {
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.switchTo().frame("itarget");
		Thread.sleep(2000);
		waitUntill_visibilityOfElement(transactionDetailsTab, 200);
		waitUntill_elementIsClickable(transactionDetailsTab, 200);
		clickElementUsingJS(transactionDetailsTab);
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(By.name("deliveryType_t"), "Deliver to Destination");
		Thread.sleep(3000);
		
		waitUntill_visibilityOfElement(saveBtn, 200);
		clickElementUsingJS(saveBtn);
	}
	public void oktaLoginAndGettingTransactionName() throws IOException, InterruptedException {
		Thread.sleep(9000);
		moveToElement(cpqLinesSSO_Button);
		waitUntill_elementIsClickable(cpqLinesSSO_Button, 200);
		clickElement(cpqLinesSSO_Button);
		Thread.sleep(5000);
		driver.switchTo().frame("itarget");
		Thread.sleep(4000);
		waitUntill_elementIsClickable(oktaUserID_TextBox, 200);
		SendTextIntoTextBox(oktaUserID_TextBox, "AKumar@C-SGROUP.COM");
		Thread.sleep(4000);
		SendTextIntoTextBox(oktaPasswd_TextBox, "Mom@12345678901");
		waitUntill_visibilityOfElement(oktaLogin_Button, 200);
		Thread.sleep(4000);
		clickElement(oktaLogin_Button);
		Thread.sleep(4000);
		//driver.switchTo().frame("itarget");
		Thread.sleep(5000);
		waitUntill_visibilityOfElement(transactionName_TextBox, 200);
		if(driver.findElement(transactionName_TextBox).getText().isEmpty()) {
			clickElement(partnerProfile);
			String parentWindow = driver.getWindowHandle();
			Set<String> s1 = driver.getWindowHandles();
			Iterator<String> i1 = s1.iterator();
			while (i1.hasNext()) {
				String ChildWindow = i1.next();
				if (!parentWindow.equalsIgnoreCase(ChildWindow)) {
					driver.switchTo().window(ChildWindow);
					clickElement(By.id("close"));
					break;
				}
			}
			driver.switchTo().window(parentWindow);
		}
	}
	public void configureTheCCTAndAddLineItems(String projectname,String lines) throws InterruptedException, IOException {
		driver.switchTo().defaultContent();
		driver.switchTo().frame("itarget");
		waitUntill_elementIsClickable(saveBtn, 200);
		waitUntill_visibilityOfElement(saveBtn, 200);
		clickElementUsingJS(saveBtn);
		Thread.sleep(2000);
		waitUntill_visibilityOfElement(cpqaddlineitembtn, 200);
		clickElementUsingJS(cpqaddlineitembtn);
		Thread.sleep(4000);
		clickElementUsingJS(cpqcctlink);
		Thread.sleep(3000);
		waitUntill_visibilityOfElement(cubicalCurtainDrpDwn, 200);
		moveToElement(cubicalCurtainDrpDwn);
		/*
		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(cubicalCurtainDrpDwn)).perform();
		*/
		clickElementUsingJS(By.linkText("Configure Cubicle Track"));
		Thread.sleep(3000);
		waitUntill_visibilityOfElement(By.id("update"), 200);
		selectValueFromDropDownUsingValue(tracktypeDrpDwn, "6062");
		selectValueFromDropDownUsingValue(tracktypeDrpDwn, "NL6062");
		selectValueFromDropDownUsingValue(tracktypeDrpDwn, "6062");
		waitUntill_visibilityOfElement(configuratorTypDrpDwn, 200);
		selectValueFromDropDownUsingValue(configuratorTypDrpDwn, "Track");
		waitUntill_visibilityOfElement(trackColorDrpDwn, 200);
		selectValueFromDropDownUsingValue(trackColorDrpDwn, "Anodized");
		waitUntill_visibilityOfElement(shippingMethodDrpDwn, 200);
		selectValueFromDropDownUsingValue(shippingMethodDrpDwn, "FedEx");
		Thread.sleep(3000);
		driver.findElement(noOfTrackersTxtBx).sendKeys("1");
		Thread.sleep(3000);
		
		selectValueFromDropDownUsingValue(trackShapeDrpDwn, "IShape");
		waitUntill_visibilityOfElement(L1LengthFt, 200);
		driver.findElement(trackQtyTxtbx).sendKeys("1");
		Thread.sleep(5000);
		driver.findElement(L1LengthFt).sendKeys("13");
		Thread.sleep(3000);
		driver.findElement(L1LenghtIn).sendKeys("121");
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		driver.switchTo().frame("itarget");
		Thread.sleep(1000);
		driver.findElement(FloorTxtBx).click();
		Thread.sleep(5000);
		driver.findElement(FloorTxtBx).sendKeys("1");
		Thread.sleep(5000);
		driver.findElement(roomTxtBx).sendKeys("2");
		Thread.sleep(8000);
		driver.findElement(updateBtn).click();
		Thread.sleep(10000);
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		driver.switchTo().frame("itarget");
		Thread.sleep(10000);
		clickElementUsingJS(addtoTransaction);
		Thread.sleep(2000);
		clickElementUsingJS(saveBtn);
		Thread.sleep(3000);
		clickElementUsingJS(submitBtn);
		Thread.sleep(3000);
		clickElementUsingJS(priceDetails);
		
		selectValueFromDropDownUsingValue(fullfillmentDrpDwn, "Straight Through");
		Thread.sleep(3000);
		selectValueFromDropDownUsingValue(By.xpath("//select[@name='_36244118_5_fulfillmentType_line']"), "Straight Through");
		Thread.sleep(3000);
		clickElementUsingJS(saveBtn);
		Thread.sleep(3000);
		driver.findElement(reviseBtn).click();
		Thread.sleep(2000);
		
		driver.findElement(parentLineChkBx).click();
		Thread.sleep(2000);
		driver.findElement(copyLineItemsBtn).click();
		Thread.sleep(2000);
		SendTextIntoTextBox(noOfCopiestxtBx, "9");
		Thread.sleep(2000);
		driver.findElement(CpyLnItempopupOKbtn).click();
		Thread.sleep(3000);
		waitUntill_elementIsClickable(copyLineItemsBtn, 200);
		
		for(int j=1;j<=9;j++) {
			Thread.sleep(2000);
			moveToElement(overageChkBx);
			waitUntill_visibilityOfElement(parentLineChkBx, 200);
			waitUntill_elementIsClickable(parentLineChkBx, 200);
			driver.findElement(parentLineChkBx).click();
			Thread.sleep(2000);
			driver.findElement(copyLineItemsBtn).click();
			Thread.sleep(2000);
			SendTextIntoTextBox(noOfCopiestxtBx, "10");
			Thread.sleep(2000);
			driver.findElement(CpyLnItempopupOKbtn).click();
			Thread.sleep(4000);
			/*
			waitUntill_elementIsClickable(copyLineItemsBtn, 200);
			Thread.sleep(2000); */
		}
		
		ConfigPrice = driver.findElement(By.id("readonly_1_configuredPrice_PriceCal_t")).getText();
		System.out.println(projectname+" Configured price is ......."+ConfigPrice);
		Thread.sleep(3000);
	}
	public String timeStampDiff(String dateStart,String dateStop) throws ParseException {
		    SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		    Date d1 = null;
		    Date d2 = null;
		    d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
		    // Get msec from each, and subtract.
		    long diff = d2.getTime() - d1.getTime();
		    long diffSeconds = diff / 1000 % 60;
		    long diffMinutes = diff / (60 * 1000) % 60;
		    long diffHours = diff / (60 * 60 * 1000);
		    System.out.println("Time in seconds: " + diffSeconds + " seconds.");
		    System.out.println("Time in minutes: " + diffMinutes + " minutes.");
		    System.out.println("Time in hours: " + diffHours + " hours.");
		    return diffHours + " hours."+diffMinutes + " minutes."+diffSeconds + " seconds.";
	}
	public void commisionTab(String projectname) throws Exception {
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.switchTo().frame("itarget");
		Thread.sleep(2000);
		waitUntill_visibilityOfElement(CommisionsTab, 200);
		clickElementUsingJS(CommisionsTab);
		Thread.sleep(3000);
		String QuoteTotal = driver.findElement(By.id("quoteTotal_quote")).getAttribute("value");
		System.out.println(projectname+" Quote price is ......."+QuoteTotal);
		Thread.sleep(2000);
		Thread.sleep(2000);
		if(driver.findElement(By.id("territoryRepID_quote")).getText().isEmpty()) {
			driver.findElement(By.id("territoryRepID_quote")).sendKeys("112");
		}
		selectValueFromDropDownUsingValue(isCommissionable, "Yes");
		Thread.sleep(3000);
		clickElementUsingJS(submitBtn);
		Thread.sleep(3000);
		clickElementUsingJS(saveBtn);
		Thread.sleep(3000);
		clickElementUsingJS(recievePO);
		Thread.sleep(2000);
		
		if(ConfigPrice.equalsIgnoreCase(QuoteTotal)) {
			System.out.println(projectname+" Commision value is same  " +ConfigPrice );
		}
		Thread.sleep(3000);
		Thread.sleep(3000);
		if(driver.findElement(By.id("territoryRepID_quote")).getText().isEmpty()) {
			driver.findElement(By.id("territoryRepID_quote")).sendKeys("112");
		}
		String commisiontime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		System.out.println(projectname+" click commision btn . .. . ."+commisiontime);
		Reporter.log(projectname+" click commision btn . .. . ."+commisiontime);
		log.info(projectname+" click commision btn . .. . ."+commisiontime);
		clickElementUsingJS(CalculateCommisionBtn);
		scrollupAndDown(refreshlink);
		
		String commisiontime1 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		System.out.println(projectname+ "time taken to calculate commision . .. . ."+commisiontime1);
		Reporter.log(projectname+ "time taken to calculate commision . .. . ."+commisiontime1);
		log.info(projectname+ "time taken to calculate commision . .. . ."+commisiontime1);
		Reporter.log(projectname+" Time taken for calculating the commision "+timeStampDiff(commisiontime,commisiontime1));
		System.out.println(projectname+" Time taken for calculating the commision "+timeStampDiff(commisiontime,commisiontime1));
		Thread.sleep(3000);
		waitUntill_elementIsClickable(CommisionsTab, 200);
		clickElementUsingJS(CommisionsTab);

		String EstComm = driver.findElement(EstimatedCommision).getAttribute("value");
		System.out.println(projectname+" Estimated Commision.....  " + EstComm);
		Thread.sleep(3000);
	}
	
	
	public void mouseOverandclick(By mouseover,By clickelemnt) {
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(mouseover)).perform();
		act.moveToElement(driver.findElement(clickelemnt)).click().build().perform();
	}
	
	public void addNewOppRelAcc(String AccRole,String Contact,String Accountnam) throws InterruptedException {
		 Thread.sleep(2000);
		driver.findElement(newOppRelAcc).click();
		 Thread.sleep(2000);
		 selectValueFromDropDownUsingValue(accRole, AccRole);
		 Thread.sleep(2000);
		 driver.findElement(AccntTxtbox).sendKeys(Accountnam);
		 Thread.sleep(2000);
		 driver.findElement(By.xpath("//div[@class='requiredInput']/span/a/img")).click();
		 Thread.sleep(2000);
		 switchingWindowAndHand("resultsFrame", By.xpath("//div[@class='pbBody']/table//tr/th"), Accountnam);
		 Thread.sleep(2000);
		 selectValueFromDropDownUsingVisibleText(ContactDrop, Contact);
		 Thread.sleep(2000);
		 driver.findElement(By.xpath("(//input[@value='Save'])[1]")).click();
	}

	public void scrollupAndDown(By refreBtn) throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("window.scrollBy(0,400)");
		Thread.sleep(4000);
		executor.executeScript("window.scrollBy(0,-400)");
		for (int i = 0; i <= 300; i++) {
			Thread.sleep(5000);
			if (driver.findElement(refreBtn).isDisplayed()) {
				executor.executeScript("arguments[0].click();", driver.findElement(refreBtn));
				break;
			} else {
				Thread.sleep(3000);
				executor.executeScript("window.scrollBy(0,500)");
				Thread.sleep(2000);
				executor.executeScript("window.scrollBy(0,-500)");
			}
		}
	}
	
	public void salesForceLogin() throws InterruptedException {
		driver.findElement(username).sendKeys(pro.getProperty("repUserName"));
		driver.findElement(password).sendKeys(pro.getProperty("RepPassword"));
		driver.findElement(signInBtn).click();
		log.info("Logged into salesforce");
	}
	
	public void loginwithCSUser(String CSUser) throws IOException, InterruptedException {
		waitUntill_visibilityOfElement(hp_search_txt, 200);
		driver.findElement(hp_search_txt).sendKeys(CSUser);
		Thread.sleep(2000);
		clickElement(By.id("phSearchButton"));
		Thread.sleep(6000);
		if(driver.findElement(By.xpath("//img[@title='"+CSUser+"']/ancestor::th[1]/div/div[@title='"+CSUser+"']")).isDisplayed()){
			super.clickElementUsingJS(By.xpath("//img[@title='"+CSUser+"']/ancestor::th[1]/div/div[@title='"+CSUser+"']"));
			//clickElementUsingJS(By.xpath("//img[@title='"+CSUser.trim()+"']/ancestor::th[1]/div/div[@title='"+CSUser.trim()+"']"));
		}
		else {
			clickElement(By.id("phSearchButton"));
		}
		clickElement(By.xpath("//img[@title='"+CSUser+"']/ancestor::th[1]/div/div/a[text()='"+CSUser+"']"));
		waitUntill_visibilityOfElement(By.id("moderatorMutton"), 200);
		waitUntill_elementIsClickable(By.id("moderatorMutton"), 200);
		
		driver.findElement(By.id("moderatorMutton")).click();
		waitUntill_visibilityOfElement(By.id("USER_DETAIL"), 200);
		driver.findElement(By.id("USER_DETAIL")).click();
		waitUntill_visibilityOfElement(By.name("login"), 200);
		waitUntill_elementIsClickable(By.name("login"), 200);
		driver.findElement(By.name("login")).click();
	}
}
