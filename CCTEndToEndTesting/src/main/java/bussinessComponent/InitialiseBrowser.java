package bussinessComponent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import com.google.common.io.Files;

import io.github.bonigarcia.wdm.WebDriverManager;
import pageElements.SuperScreen;

public abstract class InitialiseBrowser implements SuperScreen{
	public WebDriver driver;
	public Properties pro = new Properties();
	public String path_propertyFile = System.getProperty("user.dir")+"\\Config.properties";
	Logger log= Logger.getLogger(InitialiseBrowser.class);
	
	public void CalBrowser() throws IOException {
		FileInputStream file = new FileInputStream(path_propertyFile);
		pro.load(file);
		
		switch (pro.getProperty("Browser")) {
		case "Chrome Headless":
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+pro.getProperty("pathofChrome"));
			ChromeOptions options = new ChromeOptions();
	        options.addArguments("headless");
	        driver = new ChromeDriver(options);
			//driver = new ChromeDriver();
			System.out.println("------------------------------------Chrome Browser initiated---------------------------------------");
			getURL_Maxmise();
			break;
		case "Chrome":
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			log.info("launching chrome broswer");
			System.out.println("------------------------------------Chrome Browser initiated---------------------------------------");
			getURL_Maxmise();
			break;
		case "FireFox":
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+pro.getProperty("pathofFireFox"));
			driver = new FirefoxDriver();
			System.out.println("------------------------------------Mozilla Browser initiated---------------------------------------");
			getURL_Maxmise();
			break;
		case "headless":
			 driver = new HtmlUnitDriver();
			 System.out.println("------------------------------------Headless Browser initiated---------------------------------------");
			 break;
		default:
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+pro.getProperty("pathofIE"));
			driver = new InternetExplorerDriver();
			getURL_Maxmise();
			break;
		}
	}
	public void getURL_Maxmise() {
		driver.get(pro.getProperty("url"));
		driver.manage().window().maximize();
	}
	
	
	@AfterTest
	public void driverclose() {
		if(driver != null)
		{
			System.out.println("----------------------------Browser Closing  ---------------------------------------");
			driver.quit();
			
		}
	}
	
	public void selectValueFromDropDownUsingIndex(By element, int index) throws InterruptedException {
		Thread.sleep(2000);
 		Select sel = new Select(driver.findElement(element));
 		sel.selectByIndex(index);
	}
	public void selectValueFromDropDownUsingValue(By element, String value) throws InterruptedException {
		Select sel = new Select(driver.findElement(element));
 		sel.selectByValue(value);
 		Thread.sleep(2000);
	}
	public void selectValueFromDropDownUsingVisibleText(By element, String text) throws InterruptedException {
		Thread.sleep(3000);
		Select sel = new Select(driver.findElement(element));
 		sel.selectByVisibleText(text);
	}
	public void clickLinkText(String text) {
		WebElement ele = driver.findElement(By.linkText(text));
		if(ele != null) {
			ele.click();
		} else {
			System.out.println("Link Not Available");
		}
		
	}

	public void clickTab(String text) {
		WebElement ele = driver.findElement(By.xpath("//li/a[contains(text(),"+text+")]"));
		if(ele != null) {
			ele.click();
		} else {
			System.out.println("Tab Not Available");
		}
	}
	
	public void clickrecordFromTable(By elements, String record) {
		List<WebElement> li = driver.findElements(elements);
		for(WebElement records : li) {
			if(records.getText().contains(record)) {
				records.click();
				break;
			}
		}
	}
	public void takeScreenShoot(String methodName) throws IOException {
		File file = new File(System.getProperty("user.dir")+"\\src\\main\\java\\com\\SalesForce\\ScreenShots\\");
		file.mkdir();
		File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		Files.copy(screenshotFile, new File(file+"\\"+methodName+".png"));
	}
	
}
