package superScreen;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import bussinessComponent.BussinessComponent;
import bussinessComponent.ReadExcelData;
import jxl.read.biff.BiffException;
import pageElements.Project;

public class LS1224 extends BussinessComponent implements Project{
	
	@Test(priority=1)
	public void repSearch() throws IOException, BiffException, InterruptedException {
		CalBrowser();
		salesForceLogin();
		loginwithCSUser("Jeffrey Arthur");
		Thread.sleep(2000);
		ReadExcelData read =new ReadExcelData("ls1224.xls", "Sheet1");
		createoppandQuote("TESTCSGSO_LS1224_Opp14","TESTCSGSO_LS1224_Quote14");

	}
	
	public void createoppandQuote(String opp,String quote) throws InterruptedException {
		driver.findElement(By.linkText("CS Opportunities")).click();
		Thread.sleep(2000);
		driver.findElement(new_btn).click();
		driver.findElement(opportunityNameTXT).sendKeys(opp);
		selectValueFromDropDownUsingValue(By.id("00N3000000CJG32"), "ADS");
		selectValueFromDropDownUsingValue(projectTypeDrp_Opp, "New Construction");  //TODO
		selectValueFromDropDownUsingValue(specificationLevel,"Basis of Design");
		selectValueFromDropDownUsingValue(stageDrpDwn, "Bidding");
		driver.findElement(By.xpath("//input[@id='00N3000000B7bUw']/ancestor::span/span/a")).click();
		driver.findElement(By.xpath("//input[@id='CF00N3000000B5zS5']")).sendKeys("TESTCSGSO_LS1224");
		driver.findElement(csOppSaveBtn).click();
		
		
		//addNewOppRelAcc("Architect - Consulting","Alicia Bergey","APEM (DC Metro)");
		addNewOppRelAcc("Representative - Bidding","Jessica Boardman","APEM (DC Metro)");
		driver.findElement(newQuote).click();
		
		selectValueFromDropDownUsingVisibleText(RepInformation, "APEM (DC Metro) / Representative - Bidding / 347");
		Thread.sleep(2000);
		selectValueFromDropDownUsingVisibleText(By.id("quotePage:theForm:theBlock:customerInfo:contactSelected"), "APEM (DC Metro) / Architect - Consulting");
		
		driver.findElement(By.id("quotePage:theForm:theBlock:j_id76:j_id77")).sendKeys(quote);
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(By.id("quotePage:theForm:theBlock:j_id76:j_id86"), "Won");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(By.xpath("//select[@id='quotePage:theForm:theBlock:j_id76:j_id95']"), "New");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(By.xpath("//select[@id='quotePage:theForm:theBlock:j_id76:j_id93']"), "PTQ");
		Thread.sleep(2000);
		selectValueFromDropDownUsingValue(By.id("quotePage:theForm:theBlock:j_id76:j_id92"), "DS-ADS");
		driver.findElement(By.xpath("(//input[@value='Save'])[1]")).click();
		Thread.sleep(3000);
		
	}
}
