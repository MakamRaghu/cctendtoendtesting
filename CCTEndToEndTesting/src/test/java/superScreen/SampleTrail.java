package superScreen;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.Test;

import bsh.ParseException;
import bussinessComponent.BussinessComponent;
import jxl.read.biff.BiffException;

public class SampleTrail  extends BussinessComponent{
	
	@Test(priority=1)
	public void repSearch() throws IOException, BiffException, InterruptedException, java.text.ParseException, ParseException {
		   String dateStart = "11.03.14 09:29:58";
		    String dateStop = "11.03.14 09:33:43";

		    SimpleDateFormat format = new SimpleDateFormat("yy.MM.dd HH:mm:ss");

		    Date d1 = null;
		    Date d2 = null;
		    d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

		    // Get msec from each, and subtract.
		    long diff = d2.getTime() - d1.getTime();
		    long diffSeconds = diff / 1000 % 60;
		    long diffMinutes = diff / (60 * 1000) % 60;
		    long diffHours = diff / (60 * 60 * 1000);
		    System.out.println("Time in seconds: " + diffSeconds + " seconds.");
		    System.out.println("Time in minutes: " + diffMinutes + " minutes.");
		    System.out.println("Time in hours: " + diffHours + " hours.");
	}

}
