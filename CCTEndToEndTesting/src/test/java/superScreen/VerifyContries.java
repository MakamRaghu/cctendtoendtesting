package superScreen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import bussinessComponent.BussinessComponent;
import bussinessComponent.ReadExcelData;
import jxl.read.biff.BiffException;
import pageElements.Project;

public class VerifyContries extends BussinessComponent implements Project{
	public String projectName = "Project samplenine";
	public String oppName = "Opp samplenine";
	public String username = "Sales TestUser";
	ArrayList<String> countries = new ArrayList<String>();
	@Test(priority=1)
	public void repSearch() throws IOException, BiffException, InterruptedException {
		CalBrowser();
		salesForceLogin();
		loginwithCSUser(username.trim()); 
		waitUntill_visibilityOfElement(By.linkText("Projects"), 200);
		
	}
	
	@Test(priority=2)
	public void gotoQuotes() throws BiffException, IOException, InterruptedException {
		ReadExcelData read =new ReadExcelData("td.xls", "Sheet1");
		waitUntill_visibilityOfElement(hp_search_txt, 200);
		driver.findElement(hp_search_txt).sendKeys("001019255");
		Thread.sleep(2000);
		clickElement(By.id("phSearchButton"));
		Thread.sleep(3000);
		clickLinkText("001019255");
		waitUntill_visibilityOfElement(cpqLines, 30);
		moveToElement(cpqLines);
		Thread.sleep(3000);
		clickElement(cpqLines);
		Thread.sleep(5000);
		driver.switchTo().frame("itarget");
		waitUntill_visibilityOfElement(cpqUsername, 30);
		Thread.sleep(3000);
		SendTextIntoTextBox(cpqUsername, "SalesTestUser");
		SendTextIntoTextBox(cpqPassword, "dyEyeA5@@3");
		clickElement(cpqLoginBtn);
		Thread.sleep(7000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("itarget");
		Thread.sleep(5000);
		waitUntill_visibilityOfElement(customerDetailsTab, 30);
		clickElement(customerDetailsTab);
		Thread.sleep(10000);
		waitUntill_visibilityOfElement(By.xpath("(//a[@aria-label='expand'])[2]"), 300);
		Thread.sleep(3000);
		for(int i=75;i<=232;i++) {
			Thread.sleep(3000);
			clickElement(By.xpath("(//a[@aria-label='expand'])[2]"));
			System.out.println(read.readData(0, i));
			Thread.sleep(5000);
			driver.findElement(By.xpath("//ul[@class='oj-listbox-results']/li/div/oj-option[text()='"+read.readData(0, i).trim()+"']")).click();
			Thread.sleep(7000);
			try {
				Thread.sleep(5000);
				driver.findElement(By.id("oj-combobox-choice-shipToState_t-combobox")).isDisplayed();
				System.out.println("dropdown is displaying  for this country " + read.readData(0, i).trim());
			}
			catch(Exception e) {
				System.out.println("TextBox is displaying for this country " +read.readData(0, i).trim());
			}
			
			/*
			 * boolean elemen =
			 * driver.findElement(By.id("oj-combobox-choice-shipToState_t-combobox")).
			 * isDisplayed(); if (!elemen) { Thread.sleep(5000);
			 * System.out.println("TextBox is displaying  for this country " +
			 * read.readData(0, i).trim()); } else { Thread.sleep(5000);
			 * System.out.println("dropdown is displaying for this country " +
			 * read.readData(0, i).trim()); }
			 */
			}
			
		}
		
		/*
		clickElement(By.xpath("(//a[@aria-label='expand'])[2]"));
		Thread.sleep(3000);
		List<WebElement> country = driver.findElements(By.xpath("//ul[@class='oj-listbox-results']/li/div/oj-option"));
		System.out.println(country.size());
		int i;
		for (WebElement li : country) {
			Thread.sleep(3000);
			countries.add(li.getText());
			
			}
		System.out.println(countries);
		for (i = 1; i <= country.size(); i++) {
			if (country.get(i).equals(driver.findElement(By.xpath("//ul[@class='oj-listbox-results']/li["+i+"]/div/oj-option")).getText())) {
				driver.findElement(By.xpath("//ul[@class='oj-listbox-results']/li["+i+"]/div/oj-option")).click();
				Thread.sleep(5000);
				if (driver.findElement(By.id("oj-combobox-choice-shipToState_t-combobox")).isDisplayed()) {
					System.out.println("dropdown is displaying  for this country " + countries);
				} else {
					System.out.println("TextBox is displaying for this country " + countries);
				}
			}
			waitUntill_visibilityOfElement(By.xpath("(//a[@aria-label='expand'])[2]"), 30);
			clickElement(By.xpath("(//a[@aria-label='expand'])[2]"));
			Thread.sleep(3000);
		}
			/*
			if (!li.getText().isEmpty()) {
				countries = li.getText();
				Thread.sleep(3000);
				li.click();
				Thread.sleep(5000);
				try {
					driver.switchTo().defaultContent();
					driver.switchTo().frame("itarget");
					// SoftAssert sa = new SoftAssert();
					// sa.assertEquals(true,
					// driver.findElement(By.id("oj-combobox-choice-shipToState_t-combobox")).isDisplayed(),
					// "TextBox is displaying for this country " + countries);
					if (driver.findElement(By.id("oj-combobox-choice-shipToState_t-combobox")).isDisplayed()) {
						System.out.println("dropdown is displaying  for this country " + countries);
					} else {
						System.out.println("TextBox is displaying for this country " + countries);
					}
				} catch (Exception e) {
					e.getMessage();

				} finally {
					waitUntill_visibilityOfElement(By.xpath("(//a[@aria-label='expand'])[2]"), 30);
					clickElement(By.xpath("(//a[@aria-label='expand'])[2]"));
					Thread.sleep(3000);
				}
			}
			*/

		
	
	/*
	for(int i =1; i<=contsize;i++) {
		Thread.sleep(8000);
		clickElementUsingJS(By.xpath("//ul[@class='oj-listbox-results']/li["+i+"]/div/oj-option"));
		Thread.sleep(4000);
		try {
			if (driver.findElement(By.id("oj-combobox-choice-shipToState_t-combobox")).isDisplayed()) {
				System.out.println("dropdown is displaying  for this country " + countries);
			}
		}catch(Exception e) {
			System.out.println("TextBox is displaying for this country " + countries);
		}
		finally {
			waitUntill_visibilityOfElement(By.xpath("(//a[@aria-label='expand'])[2]"), 30);
			clickElementUsingJS(By.xpath("(//a[@aria-label='expand'])[2]"));
			Thread.sleep(3000);
		}
		 
		
	}
	*/
	@AfterTest
	public void tearDown() {
		driver.close();
	}
}
