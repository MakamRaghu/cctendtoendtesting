package superScreen;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import bussinessComponent.BussinessComponent;
import jxl.read.biff.BiffException;
import pageElements.Project;

public class Quote2OrderCreation extends BussinessComponent implements Project{
	public String projectName = "Project samplenine";
	public String oppName = "Opp samplenine";
	@Test(priority=1)
	public void repSearch() throws IOException, BiffException, InterruptedException {
		CalBrowser();
		salesForceLogin();
		loginwithCSUser("Blanca Banda"); //CCTestUser //salesforcetestuser
		waitUntill_visibilityOfElement(By.linkText("Projects"), 200);
		driver.findElement(By.linkText("Projects")).click();
		
	}
	//this is test 2
	@Test(priority=2)
	@Parameters("projName")
	public void createProject(String projName) throws BiffException, IOException, InterruptedException {
		System.out.println(projName);
		createProjects(projName);
		addNewOppRelAcc("Architect - Consulting","George Ennesser","New Jersey Sales");
	}
	@Test(priority=3)
	@Parameters({"OppName"})
	public void createOppurtunity(String OppName) throws BiffException, IOException, InterruptedException {
		System.out.println(OppName);
		Thread.sleep(4000);
	    createOpportunities(OppName);
	}
	
	@Test(priority=4)
	public void createQuote() throws BiffException, IOException, InterruptedException {
		addNewOppRelAcc("Architect - Consulting","George Ennesser","New Jersey Sales");
		Thread.sleep(2000);
		addNewOppRelAcc("Representative - Bidding","George Ennesser","New Jersey Sales");
		Thread.sleep(2000);
		addNewOppRelAcc("Representative - Specification","George Ennesser","New Jersey Sales");  //"Test Apem Rep","Test APEM Rep"
		Thread.sleep(2000);
		createQuot();
	}
	
	@Test(priority=5)
	@Parameters("projName")
	public void cpqLines(String projName) throws Exception {
		Thread.sleep(2000);
		cpqlinescreate(projName);
	}
	
	@AfterTest
	public void tearDown() {
		driver.close();
	}
}
